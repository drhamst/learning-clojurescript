### ClojureScript with React (Reagent) - A simple todo app - oxalorg

https://www.youtube.com/watch?v=tRYreGS53Z4&feature=youtu.be

### Development mode

```
npm install
npx shadow-cljs watch app
```

start a ClojureScript REPL

```
npx shadow-cljs browser-repl
```

### Building for production

```
npx shadow-cljs release app
```
